export function joinUrls(...urls: string[]): string {
    return urls.reduce((current: string, next: string) => {
        const currentEndsWithSlash = current[current.length - 1] === '/';
        const nextStartsWithSlash = next[0] === '/';

        if (currentEndsWithSlash && nextStartsWithSlash) {
            current += next.substr(1);
        } else if (!currentEndsWithSlash && !nextStartsWithSlash) {
            current += '/' + next;
        }

        return current;
    })
}

export default joinUrls;