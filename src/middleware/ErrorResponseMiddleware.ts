import {FetchMiddleware} from "../classes";

export const ErrorResponseMiddleware: FetchMiddleware = (request: RequestInit) => (response: Response) => response.ok
    ? response
    : Promise.reject(response);

export default ErrorResponseMiddleware;