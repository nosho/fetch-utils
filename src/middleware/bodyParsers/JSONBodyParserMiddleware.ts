import {FetchMiddleware} from "../../classes";

export interface JsonBodyParserMiddlewareOptions {
    parseRequest?: boolean;
    parseResponse?: boolean;
    parseErrors?: boolean;
    methods?: string[];
    requestPredicate?(request: RequestInit): boolean;
    responsePredicate?(request: RequestInit, response: Response): boolean;
}

// @todo make complete
const allowedBodyConstructors = [FormData, Blob, File, FileList];

const defaults: JsonBodyParserMiddlewareOptions = {
    parseRequest: true,
    parseResponse: true,
    parseErrors: true,
    methods: ["get", "put", "post"],
    requestPredicate() {
        return true;
    },
    responsePredicate() {
        return true;
    },
};

export function JSONBodyParserMiddleware(options: JsonBodyParserMiddlewareOptions = {}): FetchMiddleware {
    options = {...defaults, ...options};

    return function JsonBodyParser(request: RequestInit) {
        const eligible = typeof request.body === "object" && allowedBodyConstructors.indexOf(request.body.constructor) !== -1
        const enabled = options.requestPredicate(request) && options.parseRequest;

        if (enabled && eligible) request.body = JSON.stringify(request.body);

        return (response: Response) => {
            const enabled = options.responsePredicate(request, response) && options.parseResponse && (options.parseErrors || response.ok);
            // @todo check method case insensitive
            const eligible = response instanceof Response && options.methods.indexOf(request.method) !== -1;

            if (enabled && eligible) return response.json();

            return response;
        }
    }
}

export default JSONBodyParserMiddleware;