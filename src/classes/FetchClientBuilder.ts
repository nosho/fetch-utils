import {FetchClient, FetchMiddleware} from "./FetchClient";

export class FetchClientBuilder {

    protected baseUrl: string = "";
    protected middleware: FetchMiddleware[] = [];
    protected requestInit: RequestInit = {headers: {}} as RequestInit;

    public setBaseUrl(baseUrl: string): this {
        this.baseUrl = baseUrl;
        return this;
    }

    public clone(): FetchClientBuilder {
        const builder = new FetchClientBuilder();
        builder.setBaseUrl(this.baseUrl)
        builder.set(this.requestInit);
        builder.addMiddleware(...this.middleware);
        return builder;
    }

    public set(requestInit: RequestInit): this {
        this.requestInit = {
            ...this.requestInit,
            ...requestInit,
            headers: {
                ...this.requestInit.headers,
                ...(requestInit.headers || {})
            }
        };

        return this;
    }

    public addMiddleware(...middleware: FetchMiddleware[]): this {
        this.middleware.push(...middleware);
        return this;
    }

    public setHeader(headerName: string, headerValue: string | number | boolean): this {
        this.requestInit.headers = {...this.requestInit.headers, [headerName]: headerValue};
        return this;
    }

    public setHeaders(headers: { [name: string]: string | number | boolean }): this {
        this.requestInit.headers = {...this.requestInit.headers, ...headers};
        return this;
    }

    public setReferrer(referrer: string): this {
        this.requestInit.referrer = referrer;
        return this;
    }

    public setReferrerPolicy(referrerPolicy: string): this {
        this.requestInit.referrerPolicy = referrerPolicy;
        return this;
    }

    public setMode(mode: string): this {
        this.requestInit.mode = mode;
        return this;
    }

    public setCredentials(credentials: string): this {
        this.requestInit.credentials = credentials;
        return this;
    }

    public setCache(cache: string): this {
        this.requestInit.cache = cache;
        return this;
    }

    public setRedirect(redirect: string): this {
        this.requestInit.redirect = redirect;
        return this;
    }

    public setIntegrity(integrity: string): this {
        this.requestInit.integrity = integrity;
        return this;
    }

    public setKeepAlive(keepAlive: boolean): this {
        this.requestInit.keepalive = keepAlive;
        return this;
    }

    public build(): FetchClient {
        return new FetchClient(this.baseUrl, this.requestInit, this.middleware);
    }

}

export default FetchClientBuilder;