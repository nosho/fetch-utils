import {ErrorResponseMiddleware} from "../middleware";
import {JSONBodyParserMiddleware} from "../middleware/bodyParsers";
import {FetchClient} from "./FetchClient";
import {FetchClientBuilder} from "./FetchClientBuilder";

export class RestClient<TModel extends { [name: string]: any }, TId> {

    protected idProperty: string;
    protected fetchClientBuilder: FetchClientBuilder;
    protected fetchClient: FetchClient;

    constructor(baseUrl: string, idProperty: string = "id") {
        this.idProperty = idProperty;

        this.fetchClientBuilder = this.fetchClientBuilder || new FetchClientBuilder()
                .setBaseUrl(baseUrl)
                .addMiddleware(ErrorResponseMiddleware, JSONBodyParserMiddleware({parseErrors: false}))
                .setHeader("Content-Type", "application/json");

        this.fetchClient = this.fetchClient || this.fetchClientBuilder.build();
    }

    public findAll<T2 extends TModel>(): Promise<T2[]> {
        return this.fetchClient.get<T2[]>();
    }

    public findById<T2 extends TModel>(id: TId): Promise<T2> {
        return this.fetchClient.get<T2>(`/${id}`);
    }

    public save<T2 extends TModel>(model: Partial<T2>): Promise<T2> {
        if (model[this.idProperty]) {
            // update
            return this.fetchClient.put<T2>(`/${model[this.idProperty]}`, model);
        } else {
            // create
            return this.fetchClient.post<T2>(model);
        }
    }

    public delete(id: TId): Promise<void> {
        return this.fetchClient.delete<void>(`/${id}`);
    }

}

export default RestClient;