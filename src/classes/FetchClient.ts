import {joinUrls} from "../helpers/joinUrls";
export type FetchRequestMiddleware = (req: RequestInit) => void;
export type FetchResponseMiddleware = (req: Response | any) => Promise<any> | any;
export type FullFetchMiddleware = (req: RequestInit) => FetchResponseMiddleware;
export type FetchMiddleware = FetchRequestMiddleware | FullFetchMiddleware;

export class FetchClient {

    protected baseUrl: string;
    protected middleware: FetchMiddleware[];
    protected requestInit: RequestInit;

    constructor(baseUrl: string, requestInit: RequestInit, middleware: FetchMiddleware[]) {
        this.baseUrl = baseUrl;
        this.requestInit = requestInit;
        this.middleware = middleware;
    }

    public async get<T>(url: string = "", requestInit: RequestInit = {}, middleware: FetchMiddleware[] = []): Promise<T> {
        return this.execute<T>(url, {
            ...requestInit,
            method: "get"
        }, middleware);
    }

    public async post<T>(body?: any, requestInit?: RequestInit, middleware?: FetchMiddleware[]): Promise<T>;
    public async post<T>(url?: string, body?: any, requestInit?: RequestInit, middleware?: FetchMiddleware[]): Promise<T>;
    public async post<T>(url: string
        | any = "", body: any = undefined, requestInit: RequestInit = {}, middleware: FetchMiddleware[] = []): Promise<T> {
        if (typeof url === "object") {
            body = url;
            url = "";
        }

        return this.execute<T>(url, {
            ...requestInit,
            body,
            method: "post"
        }, middleware);
    }

    public async put<T>(body?: any, requestInit?: RequestInit, middleware?: FetchMiddleware[]): Promise<T>;
    public async put<T>(url?: string, body?: any, requestInit?: RequestInit, middleware?: FetchMiddleware[]): Promise<T>;
    public async put<T>(url: string
        | any = "", body: any, requestInit: RequestInit = {}, middleware: FetchMiddleware[] = []): Promise<T> {
        if (typeof url === "object") {
            body = url;
            url = "";
        }

        return this.execute<T>(url, {
            ...requestInit,
            body,
            method: "put"
        }, middleware);
    }

    public async delete<T>(url: string = "", requestInit: RequestInit = {}, middleware: FetchMiddleware[] = []): Promise<T> {
        return this.execute<T>(url, {
            ...requestInit,
            method: "delete"
        }, middleware);
    }

    protected async execute<T>(url: string, requestInit: RequestInit = {}, middleware: FetchMiddleware[] = []): Promise<T> {
        middleware = [...this.middleware, ...middleware];
        requestInit = {
            ...this.requestInit,
            ...requestInit,
            headers: {
                ...this.requestInit.headers,
                ...(requestInit.headers || {})
            }
        };

        url = joinUrls(this.baseUrl || "", url || "");

        const responseMiddleware: FetchResponseMiddleware[] = [];

        middleware.forEach(middleware => {
            const _responseMiddleware = middleware(requestInit);
            if (_responseMiddleware) responseMiddleware.push(_responseMiddleware);
        });

        const response = await fetch(url, requestInit);
        let returnValue: T = response as any;

        const runResponseMiddleware = async (index: number = 0): Promise<T> => {
            let reject: boolean = false;

            if (index < responseMiddleware.length) {
                try {
                    returnValue = await Promise.resolve(responseMiddleware[index](returnValue));
                } catch (err) {
                    reject = true;
                    returnValue = err;
                }

                returnValue = await runResponseMiddleware(index + 1);

                return reject
                    ? Promise.reject(returnValue)
                    : returnValue;
            }

            return returnValue;
        };

        return runResponseMiddleware();
    }

}

export default FetchClient;